# 使用mybatis-plus-join-boot-starter实现mybatis-plus表之间联查
### 使用方法
#### 安装
+ maven
```xml
  <dependency>
      <groupId>com.github.yulichang</groupId>
      <artifactId>mybatis-plus-join-boot-starter</artifactId>
      <version>1.3.8</version>
  </dependency>
  ```
+ Gradle
```
   implementation 'com.github.yulichang:mybatis-plus-join-boot-starter:1.3.8'
  ```
