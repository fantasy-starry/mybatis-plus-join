package com.gremlin;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.gremlin.vo.ProductInfoVo;
import com.gremlin.service.OpProductService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
class MybatisPlusJoinDemoApplicationTests {

    @Autowired
    private OpProductService productService;

    @Test
    void contextLoads() {
        List<ProductInfoVo> productDTOS = productService.queryAllProduct();
        log.info(productDTOS.toString());
    }

    @Test
    void contextLoads1() {
        List<ProductInfoVo> productDTOS = productService.queryAllProductLambda();
        log.info(productDTOS.toString());
    }

    @Test
    void contextLoads2() {
        IPage<ProductInfoVo> productInfoVoIPage = productService.queryPageProduct(1, 1);
        List<ProductInfoVo> records = productInfoVoIPage.getRecords();
        log.info(records.toString());
    }

}
