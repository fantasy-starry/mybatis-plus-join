-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        8.0.31 - MySQL Community Server - GPL
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  12.2.0.6576
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 mybatis_join 的数据库结构
CREATE DATABASE IF NOT EXISTS `mybatis_join` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mybatis_join`;

-- 导出  表 mybatis_join.product 结构
CREATE TABLE IF NOT EXISTS `product` (
  `id` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '产品id',
  `type` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '差评系统类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- 正在导出表  mybatis_join.product 的数据：~2 rows (大约)
DELETE FROM `product`;
INSERT INTO `product` (`id`, `type`) VALUES
	('adjladjlsjldsvldvlkndkscldsncds', 'ios'),
	('fdjdsjfdsjdsjfvdsjjdjfdjdsjcdsvds;\'vd;v', 'Android');

-- 导出  表 mybatis_join.product_info 结构
CREATE TABLE IF NOT EXISTS `product_info` (
  `id` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '产品信息id',
  `product_id` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '产品id',
  `name` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '产品名称',
  `price` decimal(10,4) DEFAULT NULL COMMENT '产品价格',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

-- 正在导出表  mybatis_join.product_info 的数据：~2 rows (大约)
DELETE FROM `product_info`;
INSERT INTO `product_info` (`id`, `product_id`, `name`, `price`) VALUES
	('cdscsdlvlsdvl;sdlvlds', 'adjladjlsjldsvldvlkndkscldsncds', '苹果13', 8.0000),
	('fdjdsjfdsjdsjfvdsjjdjfdjdsjcdsdscvds;\'vd;v', 'fdjdsjfdsjdsjfvdsjjdjfdjdsjcdsvds;\'vd;v', '华为', 8.0000),
	('fdjdsjfdsjdsjfvdsjjdjfdjdsjcdsvds', 'fdjdsjfdsjdsjfvdsjjdjfdjdsjcdsvds;\'vd;v', 'OPPO', 4.0000),
	('vsdmv;ldsmv;dsml;vd', 'adjladjlsjldsvldvlkndkscldsncds', '苹果15', 9.0000);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
