package com.gremlin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@TableName("product_info")
public class OpProductInfo implements Serializable {

    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    private String productId;
    private String name;
    private BigDecimal price;
}