package com.gremlin.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.yulichang.base.MPJBaseServiceImpl;
import com.github.yulichang.query.MPJQueryWrapper;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import com.gremlin.vo.ProductInfoVo;
import com.gremlin.entity.OpProduct;
import com.gremlin.entity.OpProductInfo;
import com.gremlin.mapper.OpProductMapper;
import com.gremlin.service.OpProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @className: OpProductServiceImpl
 * @author: gremlin
 * @version: 1.0.0
 * @description:
 * @date: 2022/11/28 10:54
 */
@Service
@Slf4j
public class OpProductServiceImpl extends MPJBaseServiceImpl<OpProductMapper,OpProduct> implements OpProductService {

    @Autowired
    private OpProductMapper productMapper;

    /**
     * lambda表达式查询
     */
    @Override
    public List<ProductInfoVo> queryAllProductLambda() {
        MPJLambdaWrapper mpjLambdaWrapper = new MPJLambdaWrapper<ProductInfoVo>()
                //查询表1的全部字段
                .selectAll(OpProductInfo.class)
                //查询表2的全部字段
                // .selectAll(OpProduct.class)
                //起别名
                .select(OpProduct::getId)
                .selectAs(OpProduct::getType,ProductInfoVo::getType)
                // .selectAs(OpProduct::getType,"type")
                //左查询表2条件 为表1的productId = 表2的id
                .leftJoin(OpProductInfo.class, OpProductInfo::getProductId, OpProduct::getId)
                ;
        return productMapper.selectJoinList(ProductInfoVo.class, mpjLambdaWrapper);
    }

    /**
     * 普通QueryWrapper
     */
    @Override
    public List<ProductInfoVo> queryAllProduct() {
        return productMapper.selectJoinList(ProductInfoVo.class,
                new MPJQueryWrapper<OpProduct>()
                        .selectAll(OpProduct.class)
                        .select("t2.id","t2.product_id as productId","t2.name","t2.price")
                        .leftJoin("product_info AS t2 on t2.product_id = t.id")
                        // .eq("t.status", "3")
                        // .orderByAsc()
        );
    }

    /**
     * 分页
     */
    @Override
    public IPage<ProductInfoVo> queryPageProduct(Integer pageNo, Integer pageCount) {
        MPJLambdaWrapper lambdaWrapper = new MPJLambdaWrapper<ProductInfoVo>()
                //查询表1的全部字段
                .selectAll(OpProduct.class)
                //查询表2的全部字段
                .selectAll(OpProductInfo.class)
                //左查询表2条件为表二的productId=表一的id
                .leftJoin(OpProductInfo.class, OpProductInfo::getProductId, OpProduct::getId);
        Page<ProductInfoVo> productInfoVoPage = new Page<>(pageNo, pageCount);
        return productMapper.selectJoinPage(productInfoVoPage,ProductInfoVo.class,lambdaWrapper);
    }
}
